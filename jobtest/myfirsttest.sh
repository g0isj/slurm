#!/bin/sh
# .bashrc
    # Source global definitions
if [ -f /etc/bashrc ]; then
   . /etc/bashrc
fi
    # User specific aliases and functions
module load centos6/python-2.7.3 >& /dev/null
module load centos6/ipython-0.13.2_python-2.7.3 >& /dev/null

