#!/bin/bash
SBATCH --partition=CLUSTER
SBATCH --nodes=4
SBATCH --ntasks-per-node=1
SBATCH --mem=1
SBATCH --time=2:00:00
SBATCH --output=slurm.%N.%j.out
SBATCH --error=slurm.%N.%j.err
SBATCH --mail-type=END
SBATCH --mail-type=FAIL
SBATCH --mail-user=my_email@harvard.edu

while [ 1 ] ; do
    # Force some computation even if it is useless to actually work the CPU
    echo $((13**99)) 1>/dev/null 2>&1
done

echo "Done - $HOSTNAME\n" >> /mnt/nfs/home/done
