#!/bin/bash
#SBATCH --partition=CLUSTER
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=1
#SBATCH --mem=1
#SBATCH --time=2:00:00
#SBATCH --output=slurm.%N.%j.out
#SBATCH --error=slurm.%N.%j.err

cd /mnt/nfs/home
./num.sh

